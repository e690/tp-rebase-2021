# Rapport

### Démarrage

Commande utilisées :

```
git remote add personal https://gitlab.com/e690/tp-rebase-2021.git
```

### Votre Branche

Commande utilisées :

```
git checkout -b 1-customize-readme
git commit -am "feat: modify Readme"
git push --set-upstream origin 1-customize-readme => 403 forbidden
git push --set-upstream personal 1-customize-readme

branche 1-customize-readme supprimée, issue fermée
```

### Merge Request

Commande utilisées :

```
git checkout -b main --track personal/main
git pull depuis la branche main
```